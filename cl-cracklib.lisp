;;;; $Id$
;;;; $Source$

;;;; See LICENSE for licensing information.

(in-package :cl-cracklib)

(defvar *dictionary-path* "/var/cache/cracklib/cracklib_dict")

(uffi:load-foreign-library
 (uffi:find-foreign-library '("libcrack") '("/usr/lib/" "/usr/local/lib/")
                            :types '("so" "a")))

(uffi:def-function ("FascistCheck" %FascistCheck%)
    ((passwd :cstring)
     (dictpath :cstring))
  :returning :cstring)

(defun fascistcheck (password &optional (dictionary-path *dictionary-path*))
  "Return t if `password' is a good password and nil if it is not.
When nil is returned, the second value of fascistcheck an error
diagnostics string."
  (uffi:with-cstring (c-password password)
    (let ((diag-string (%fascistcheck% c-password dictionary-path)))
      (if diag-string ; then the password is not good
          (values nil diag-string)
          (values t nil)))))