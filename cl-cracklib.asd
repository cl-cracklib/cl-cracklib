;;;; $Id: cl-cracklib.asd,v 1.2 2003/12/03 00:38:45 eenge Exp $
;;;; $Source: /project/cl-cracklib/cvsroot/cl-cracklib/cl-cracklib.asd,v $

;;;; See the LICENSE file for licensing information.

(in-package #:cl-user)

(defpackage #:cl-cracklib-system
    (:use #:cl #:asdf))

(in-package #:cl-cracklib-system)

(defsystem cl-cracklib
    :name "cl-cracklib"
    :author "Erik Enge"
    :version "0.9.1"
    :licence "MIT"
    :description "Common Lisp interface to cracklib"
    :depends-on (:uffi)
    :components ((:file "package")
                 (:file "cl-cracklib"
                        :depends-on ("package"))))
