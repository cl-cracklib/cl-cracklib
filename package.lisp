;;;; $Id$
;;;; $Source$

;;;; See the LICENSE file for licensing information.

(in-package :cl-user)

(eval-when (:execute :load-toplevel :compile-toplevel)
  (defpackage :cl-cracklib
      (:use :cl)
    (:nicknames :cracklib)
    (:export :fascistcheck
             :*dictionary-path*)))