;;;; $Id: cl-cracklib-test.asd,v 1.1 2003/12/03 00:38:46 eenge Exp $
;;;; $Source: /project/cl-cracklib/cvsroot/cl-cracklib/test/cl-cracklib-test.asd,v $

;;;; See the LICENSE file for licensing information.

(in-package #:cl-user)

(defpackage #:cl-cracklib-test-system
    (:use #:cl #:asdf))

(in-package #:cl-cracklib-test-system)

(defsystem cl-cracklib-test
    :name "cl-cracklib-test"
    :author "Erik Enge"
    :version "0.9.1"
    :licence "MIT"
    :description "Tests for cl-cracklib"
    :depends-on (:rt :cl-cracklib)
    :components ((:file "package")
                 (:file "test-cl-cracklib"
                        :depends-on ("package"))))
