;;;; $Id: cl-cracklib.lisp,v 1.1.1.1 2003/12/02 10:45:59 eenge Exp $
;;;; $Source: /project/cl-cracklib/cvsroot/cl-cracklib/cl-cracklib.lisp,v $

;;;; See LICENSE for licensing information.

(in-package :cl-cracklib-test)

(deftest fascistcheck.1 (cracklib:fascistcheck "mypassword")
  nil "it is based on a dictionary word")
(deftest fascistcheck.2 (cracklib:fascistcheck "a")
  nil "it's WAY too short")
(deftest fascistcheck.3 (cracklib:fascistcheck "aaaaa")
  nil "it is too short")