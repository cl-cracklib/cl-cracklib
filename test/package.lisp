;;;; $Id$
;;;; $Source$

;;;; See the LICENSE file for licensing information.

(in-package :cl-user)

(eval-when (:execute :load-toplevel :compile-toplevel)
  (defpackage :cl-cracklib-test
      (:use :cl :rt)
    (:nicknames :cl-cracklib-test)
    (:export :do-tests)))
